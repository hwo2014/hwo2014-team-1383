import json
import socket
import sys


class NoobBot(object):

	def __init__(self, socket, name, key):
		self.socket = socket
		self.name = name
		self.key = key
		self.color = None

	def msg(self, msg_type, data):
		self.send(json.dumps({"msgType": msg_type, "data": data}))

	def send(self, msg):
		self.socket.send((msg + "\n").encode())

	def join(self):
		return self.msg("join", {"name": self.name,
								 "key": self.key})

	def throttle(self, throttle):
		self.msg("throttle", throttle)
		self.current_throttle = throttle

	def ping(self):
		self.msg("ping", {})

	def run(self):
		self.join()
		self.msg_loop()

	def on_join(self, data):
		print("Joined")
		self.ping()

	def on_my_car(self, data):
		self.color = data["color"]
		print("My car is %s" % (self.color))

	def on_game_start(self, data):
		print("Race started")
		self.ping()

	def on_car_positions(self, data):
		my_car = None

		for car in data:
			if car["id"]["color"] == self.color:
				my_car = car
				break

		assert my_car != None

		angle = my_car["angle"]
		if angle > 0.0:
			self.throttle(max(0.5 - angle / 40, 0))
		else:
			self.throttle(0.70)

		if angle > 20:
			print("Angle is %.2f" % (angle));

		# 30 degrees @ 0.65 throttle
		# 38 degrees @ 0.67 throttle 8.97

	def on_crash(self, data):
		print("Someone crashed")
		self.ping()

	def on_game_end(self, data):
		print("Race ended")
		self.ping()

	def on_error(self, data):
		print("Error: {0}".format(data))
		self.ping()

	def msg_loop(self):
		msg_map = {
			'join': self.on_join,
			'yourCar': self.on_my_car,
			'gameStart': self.on_game_start,
			'carPositions': self.on_car_positions,
			'crash': self.on_crash,
			'gameEnd': self.on_game_end,
			'error': self.on_error,
		}
		socket_file = s.makefile()
		line = socket_file.readline()
		while line:
			msg = json.loads(line)
			msg_type, data = msg['msgType'], msg['data']
			if msg_type in msg_map:
				msg_map[msg_type](data)
			else:
				print("Got {0}".format(msg_type))
				self.ping()
			line = socket_file.readline()


if __name__ == "__main__":
	if len(sys.argv) != 5:
		print("Usage: ./run host port botname botkey")
	else:
		host, port, name, key = sys.argv[1:5]
		print("Connecting with parameters:")
		print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
		s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		s.connect((host, int(port)))
		bot = NoobBot(s, name, key)
		bot.run()
